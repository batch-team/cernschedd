## Client side of WTFIS service

Provides three functions:

  * Called internally by condor_submit to dump the user's schedd classads

  * Allow the user to bump to another schedd

  * Allow the admin to perform various various operations

The first function replaces the 'cernbatchsubmit' tool.
