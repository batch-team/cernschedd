package main

import "gitlab.cern.ch/batch-team/cernschedd/cmd"

func main() {
	cmd.Execute()
}
