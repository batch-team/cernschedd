package core

import (
	"github.com/spf13/viper"
	"github.com/hashicorp/logutils"
	"os"
	"log"
)

// config hashilogger from debug flag writing to stderr
func SetupLogger() {
	var loglevel string
	if viper.GetBool("debug") {
		loglevel = "DEBUG"
	} else {
		loglevel = "INFO"
	}
	filter := &logutils.LevelFilter{
		Levels: []logutils.LogLevel{"DEBUG", "INFO", "ERROR"},
		MinLevel: logutils.LogLevel(loglevel),
		Writer: os.Stderr,
	}
	log.SetOutput(filter)
}
